# Torrents setup

Setting up transmission and flexget.

Configuration for both is kept in separate volumes.
Initially I'd hoped to add config files when building the docker images,
but both add extra data to the config directory that would be lost when recreating the image.
This means that the initial setup does involve setting up the config files.

## Initial Setup

First thing to do is start and stop the stack:

```
$ docker-compose up -d
$ docker-compose down
```

This allows initial setup of volumes and settings files.

## Transmission

Browse to the `transmission_config` volume and edit the `settings.json` file.
These are required, but change whatever else you need:

```
{
    "download-dir": "/transmission/downloads",
    "incomplete-dir": "/transmission/incomplete",
    "incomplete-dir-enabled": true,
    "rpc-authentication-required": true,
    "rpc-enabled": true,
    "rpc-password": "<>",
    "rpc-whitelist-enabled": false,
    "start-added-torrents": true,
    "trash-original-torrent-files": true,
    "watch-dir": "/transmission/watch",
    "watch-dir-enabled": true
}
```

## Flexget

Again, a sample of a sensible configuration file (excluding tasks...): 

```
templates:
  global:
    download: /flexget/torrents
    notify:
      task:
        template: html
        via:
          - email:
              from: flexget@djomp.co.uk
              to: <>
              html: yes
              smtp_host: smtp.gmail.com
              smtp_port: 587
              smtp_username: <> 
              smtp_password: <>
              smtp_tls: yes

schedules:
  - tasks: '*'
    interval:
      hours: 1

web_server:
  web_ui: yes
  base_url: /flexget
```